package com.platzi.persistenciadedatos.modelo;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ClienteDAO {
	public static void crearClienteBD(Cliente cliente) {
		Conexion conexion = new Conexion();
		
		try (Connection cn = conexion.getConnection()) {
			String query = "INSERT INTO cliente (id, nombre, direccion, celular) "
					+ "VALUES(?, ?, ?, ?)";
			PreparedStatement preparedStatement = cn.prepareStatement(query);
			preparedStatement.setInt(1, cliente.getId());
			preparedStatement.setString(2, cliente.getNombre());
			preparedStatement.setString(3, cliente.getDireccion());
			preparedStatement.setInt(4, cliente.getCelular());
			
			preparedStatement.executeUpdate();
			
			System.out.println("Cliente insertado satisfactoriamente");
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
	}
	
	public static void listarClienteBD() {
		Conexion conexion = new Conexion();
		try(Connection cn = conexion.getConnection()) {
			String query = "SELECT * FROM cliente";
			PreparedStatement preparedStatement = cn.prepareStatement(query);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next()) {
				System.out.println("***********");
				System.out.println(resultSet.getInt("id"));
				System.out.println(resultSet.getString("nombre"));
				System.out.println(resultSet.getString("direccion"));
				System.out.println(resultSet.getInt("celular"));								
			}
			
			System.out.println("Se listó todos los Empleados");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public static void actualizarClienteBD(Cliente cliente) {
		Conexion conexion = new Conexion();
		try (Connection cn = conexion.getConnection()){
			String query = "UPDATE cliente SET nombre=?, direccion=?, celular=? WHERE id=?";
			PreparedStatement preparedStatement = cn.prepareStatement(query);
			preparedStatement.setString(1, cliente.getNombre());
			preparedStatement.setString(2, cliente.getDireccion());
			preparedStatement.setInt(3, cliente.getCelular());
			preparedStatement.setInt(4, cliente.getId());
						
			preparedStatement.executeUpdate();
			System.out.println("La actualización es exitosas");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public static void eliminarClienteBD(int cod) {
		Conexion conexion = new Conexion();
		try(Connection cn = conexion.getConnection()) {
			String query = "DELETE FROM cliente WHERE id=?";
			PreparedStatement preparedStatement = cn.prepareStatement(query);
			preparedStatement.setInt(1, cod);
			
			preparedStatement.executeUpdate();
			System.out.println("Se eliminó satistactoriamente");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

}
