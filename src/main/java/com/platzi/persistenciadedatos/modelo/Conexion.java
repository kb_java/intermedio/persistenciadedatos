package com.platzi.persistenciadedatos.modelo;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {
	public Connection getConnection() {
		Connection cn = null;
		try {
			//Driver
			Class.forName("oracle.jdbc.driver.OracleDriver");
			
			//Cadena de conexion
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE", "platzi", "platzi");			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return cn;
	}

}
