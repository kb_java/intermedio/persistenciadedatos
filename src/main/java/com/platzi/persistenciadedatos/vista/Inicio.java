package com.platzi.persistenciadedatos.vista;

import java.sql.Connection;
import java.util.Scanner;
import com.platzi.persistenciadedatos.modelo.Conexion;
import com.platzi.persistenciadedatos.servicio.ClienteService;

public class Inicio {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int opcion = 0;
		
		do {
			System.out.println("**************************");
			System.out.println("BIENVENIDO Al MAESTRO DE CLIETNE");
			System.out.println("1. Crear Cliente");
			System.out.println("2. Listar Cliente");
			System.out.println("3. Actualizar Cliente");
			System.out.println("4. Eliminar Cliente");
			System.out.println("0. Salir");
			
			//lleemos la opcion del usuario
			opcion = scanner.nextInt();  
			
			switch (opcion) {
			case 1:
				ClienteService.crearCliente();				
				break;
			case 2:
				ClienteService.leerCliente();
				break;
			case 3:
				ClienteService.actualizarCliente();
				break;
			case 4:
				ClienteService.eliminarCliente();
				break;
			default:				
				break;
			}
			
		}while(opcion != 0);
	}
}
