package com.platzi.persistenciadedatos.servicio;
import java.util.Scanner;
import com.platzi.persistenciadedatos.modelo.Cliente;
import com.platzi.persistenciadedatos.modelo.ClienteDAO;

public class ClienteService {
	public static void crearCliente() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese Id del Empleado");
		int id = Integer.parseInt(scanner.nextLine());
		
		System.out.println("Ingrese Nombre completo");
		String nombre = scanner.nextLine();
		
		System.out.println("Ingrese Direccion");
		String direccion = scanner.nextLine();
		
		System.out.println("Ingrese Celular");
		int celular = Integer.parseInt(scanner.nextLine());
		
		Cliente cliente = new Cliente();
		cliente.setId(id);
		cliente.setNombre(nombre);
		cliente.setDireccion(direccion);
		cliente.setCelular(celular);
		
		ClienteDAO.crearClienteBD(cliente);
	}
	
	public static void leerCliente() {
		ClienteDAO.listarClienteBD();
	}
	
	public static void actualizarCliente() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese el codido del Cliente");
		int cod = Integer.parseInt(scanner.nextLine());
		
		System.out.println("Ingrese Nombre completo");
		String nombre = scanner.nextLine();
		
		System.out.println("Ingrese direccion");
		String direccion = scanner.nextLine();
		
		System.out.println("Ingrese nro celular");
		int celular = Integer.parseInt(scanner.nextLine());
		
		Cliente cliente = new Cliente();
		cliente.setId(cod);
		cliente.setNombre(nombre);
		cliente.setDireccion(direccion);
		cliente.setCelular(celular);
		
		ClienteDAO.actualizarClienteBD(cliente);
	}
	
	public static void eliminarCliente() {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Ingrese el codigo del cliente");
		int cod = Integer.parseInt(scanner.nextLine());
		
		ClienteDAO.eliminarClienteBD(cod);
					
	}

}
